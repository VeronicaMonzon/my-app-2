import React from 'react';
import logo from '../imagenes/MYtineraryLogo.png';
import footer from '../imagenes/homeIcon.png';
import flecha from '../imagenes/circled-right-2.png'
function Landing() {
     
    return ( <div>
             <header className="App-header">
             <img src={logo} className="App-logo" alt="logo" />
             <p>Find your perfect trip, designed by insiders who know and love their cities</p>
             </header>
  
               <div className="container text-center">
                  <h1>Start browsing</h1>
                  <img src={flecha} className="Boton" alt="flecha" />
                  <h4>Want to build your own Mytinerary?</h4>
                </div>
                <div className="d-flex justify-content-around">
                    <a href="login.html">Login</a>
                    <a href="account.html">Create Account</a>
                </div> 
            
                <footer className="container">
                    <img src={footer} className="Boton" alt="boton" />
                </footer>
             </div>    
    );
}
export default Landing;